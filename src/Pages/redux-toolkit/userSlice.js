import { createSlice } from "@reduxjs/toolkit";
import { userLocalService } from "../../service/localService"

const initialState = {
    user: userLocalService.get(),
};
const userSlice = createSlice({
    name: "userSlice",
})